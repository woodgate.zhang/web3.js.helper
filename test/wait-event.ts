import { EventEmitter } from 'events';
export interface TestResult {
    flag: boolean;
    message: string;
}
export class WaitForEvent {
    private _em: EventEmitter;
    private _eventsListened: boolean;
    private readonly _events: Map<string, boolean>;
    constructor() {
        this._em = new EventEmitter();
        this._eventsListened = false;
        this._events = new Map<string, boolean>();
        this._events.set('ok', true);
        this._events.set('fail', false);
    }
    addEvent(event: string, value: boolean) {
        this._events.set(event, value);
    }
    ticktock(ms = 10000): Promise<TestResult> {
        return new Promise(resolve => {
            const duration = ms <=0 ? 99999999 : ms;
            const timeout = setTimeout(() => {
                resolve({ flag: false, message: 'timeout reached' });
            }, duration);
            // in case ticktock many times caused the warning of MaxListenersExceededWarning
            if (this._eventsListened === false) {
                for (let [event, value] of this._events.entries()) {
                    this._em.on(event, data => {
                        clearTimeout(timeout);
                        resolve({flag: value, message: data});
                    });
                }
                this._eventsListened = true;
            }
        });
    }
    notify(event: string, data: string) {
        this._em.emit(event, data);
    }
}
