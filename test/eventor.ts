import {EventEmitter} from "events";

export class EventDispatcher {
    private static _instance: EventDispatcher;
    protected _em: EventEmitter;
    static getInstance(): EventDispatcher {
        if (!EventDispatcher._instance) {
            EventDispatcher._instance = new EventDispatcher();
        }
        return EventDispatcher._instance;
    }
    constructor() {
        this._em = new EventEmitter();
    }
    send(event: string, data: string) {
        this._em.emit(event, data);
    }
    watch(event: string, callback: any) {
        this._em.on(event, callback);
    }
}
