import Web3 from "web3";
import solc from "solc";
import fs from "fs";
import path from "path";
import { Account } from "web3/eth/accounts";
import { Tx } from "web3/eth/types";

interface ContractFileInput {
  fPath: string;
  name: string;
}
// Connect to local Ethereum node
const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://ropsten.infura.io/v3/11ae2b7ff4c04391b71dd5a196c21b0d"
  )
);

// Assumes imported files are in the same folder/local path
const findImports = path => {
  return {
    contents: fs.readFileSync(path).toString()
  };
};
const compileContract = (
  contractFileInputs: Array<ContractFileInput>
): string => {
  const inputs = {
    language: "Solidity",
    sources: {},
    settings: {
      outputSelection: {
        "*": {
          "*": ["*"]
        }
      }
    }
  };
  for (let i = 0; i < contractFileInputs.length; i++) {
    const contractFileInput = contractFileInputs[i];
    const name = contractFileInput.name;
    const source = fs.readFileSync(contractFileInput.fPath).toString();
    inputs.sources[name] = { content: source };
  }
  console.log(inputs);
  return solc.compile(JSON.stringify(inputs), findImports);
};
const assembleContractTX = async (
  byteCode: string,
  abi: Array<any>,
  params: Array<any>,
  signer: Account
): Promise<Tx> => {
  const contract = new web3.eth.Contract(abi);

  const txObject = contract.deploy({
    data: "0x" + byteCode,
    arguments: params
  });

  const gas = await txObject.estimateGas({ from: signer.address });
  console.log(`estimated gas: ${gas}`);

  return {
    data: txObject.encodeABI(),
    gas: gas,
    value: 0
  };
};

async function main() {
  // Compile the source code
  const contractFileInput = {
    fPath: path.dirname(__filename) + "/erc20.sol",
    name: "erc20"
  };
  const contractJSON = JSON.parse(compileContract([contractFileInput]));

  const byteCode =
    contractJSON.contracts["erc20"]["OrgToken"].evm.bytecode.object;
  const abi = contractJSON.contracts["erc20"]["OrgToken"].abi;

  const signer = web3.eth.accounts.privateKeyToAccount(
    "0x783377443FA45E619E1253D4B8CF81CE33E863E978BA5257413856FFF6D36B6B"
  );
  const tx = await assembleContractTX(
    byteCode,
    abi,
    ["test token", "tos", 100000000],
    signer
  );
  const txSignature = await web3.eth.accounts.signTransaction(
    tx,
    signer.privateKey
  );
  web3.eth
    .sendSignedTransaction(txSignature.rawTransaction)
    .once("transactionHash", txHash => {
      console.log(`tx hash: ${txHash}`);
    })
    .on("confirmation", (confirmationNumber, receipt) => {
      console.log(`confirmed ${confirmationNumber} times`); // success when tx has been confirmed 25 times
      if (confirmationNumber >= 24) {
        console.log(`contract deployed successfully`);
        console.log(`contract address: ${receipt.contractAddress}`);
      }
    })
    .once("receipt", receipt => {
      console.log(receipt);
    })
    .on("error", error => {
      console.error(error);
    });
}

main().catch(error => {
  console.error(error);
  process.exit(-1);
});
