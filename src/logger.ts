import { configure, getLogger, Logger } from "log4js";

export class LoggerFactory {
  private static instance: LoggerFactory;

  private constructor() {
    configure("config/log4js.json");
  }

  static getInstance() {
    if (!LoggerFactory.instance) {
      LoggerFactory.instance = new LoggerFactory();
    }
    return LoggerFactory.instance;
  }

  getLogger(category: string): Logger {
    return getLogger(category);
  }
}
