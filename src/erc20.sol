pragma solidity ^0.5.11;

import "../node_modules/@openzeppelin/contracts/token/ERC20/ERC20Burnable.sol";
import "../node_modules/@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "../node_modules/@openzeppelin/contracts/ownership/Ownable.sol";


contract OrgToken is ERC20, Ownable {

    string public name;
    string public symbol;
    uint8 public constant decimals = 6;
    uint32 public constant version = 1;

    constructor(string memory _name, string memory _symbol, uint256 _totalSupply) public {
        name = _name;
        symbol = _symbol;
        transferOwnership(msg.sender);

        _mint(msg.sender, _totalSupply * 10 ** uint256(decimals));
    }

    function mint(address _to, uint256 _amount) public onlyOwner returns (bool) {
        _mint(_to, _amount * 10 ** uint256(decimals));
        return true;
    }

    function transfers(address[] memory recipients, uint256[] memory amounts) public returns (bool) {
        require(recipients.length == amounts.length, "params error.");
        for (uint32 i = 0; i < recipients.length; ++i) {
            transfer(recipients[i], amounts[i]);
        }
        return true;
    }
}
